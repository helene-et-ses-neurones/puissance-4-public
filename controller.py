from models import *
from view import *

class ControlJeu :
    '''Contrôleur'''
    def __init__(self, joueur_a, joueur_b, grille) :
        # Récupération de la grille de jeu et des joueurs
        self.grille = grille
        self.joueur_a = joueur_a
        self.joueur_b = joueur_b
        # Initialisation de la vue
        self.initialisation_vue = ViewPreparation(self)
    def ajout_jeton_control(self, num_col, joueur) :
        '''Ajout d'un jeton. Commande utilisée depuis la
        vue lors de l'ajout d'un jeton.'''
        # On actualise l'état de la grille de jeu dans
        # le modèle
        self.grille.ajout_jeton(num_col, joueur)
        # On vérifie l'intégralité du tableau pour savoir
        # si un joueur a gagné
        self.grille.verif_ligne()
        self.grille.verif_colonne()
        self.grille.verif_diagonale_bas()
        self.grille.verif_diagonale_haut()
        # On vérifie que le tableau n'est pas plein
        self.grille.verif_egalite()
        print("Tour numéro : ", self.grille.compte_tours)
        self.grille.imprime_grille()
    
        
    
    
