from controller import ControlJeu
from models import CaseJeu, TableauJeu, Joueur
from view import ViewPreparation, ViewProbleme, ViewFinPartie, ViewGrille


if __name__ == '__main__':
    # On créé une grille de jeu et on récupère ses attributs
    # joueur_a et joueur_b
    grille_de_jeu = TableauJeu()
    premier_joueur = grille_de_jeu.joueur_a
    deuxieme_joueur = grille_de_jeu.joueur_b
    # On instancie le controleur
    c = ControlJeu(premier_joueur, deuxieme_joueur, grille_de_jeu)
    
