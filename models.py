from random import randint

AJOUT_POIDS_1 = 2
AJOUT_POIDS_2 = 10


class Joueur :
    '''Les joueurs'''
    def __init__(self, nom_joueur, ordre_joueur, type_joueur) :
        # Les deux premières conditions doivent être possible
        # uniquement si le code est modifié (classe TableauJeu(),
        # attributs joueur_a et joueur_b) 
        if ordre_joueur not in ("A","B") :
            print("Nope, l'ordre doit être A ou B.")
        elif type_joueur not in ("humain","ia") :
            print("Nope, le joueur doit être humain ou ia")
        else :
            # Le nom du joueur
            self.nom = nom_joueur
            # L'ordre de passage ("A" ou "B")
            self.ordre = ordre_joueur
            # Le type de joueur (humain ou ia)
            self.type = type_joueur


class CaseJeu :
    '''Peut etre vide, contenir un jeton rouge (joueur A) 
    ou jaune (joueur B)'''
    def __init__(self, poids) :
        # Les cases sont toutes vides à l'instanciation
        self.etat = "v"
        self.poids = poids
        self.priorite = 0
    # Fonction inutile ??
    def __repr__ (self) :
        return str(self.etat)
    def changement_etat(self, joueur) :
        '''Change l'etat d'une case'''''
        if joueur == "A" :
            # Le joueur A joue toujours avec les jetons rouges
            self.etat = "R"
        elif joueur == "B" :
            # Le joueur A joue toujours avec les jetons rouges
            self.etat = "J"
        else :
            # On peut décommenter la ligne ci-dessous si
            # on n'utilise pas d'interface graphique
            #print("mauvais nom de joueur, pas de changement")
            pass
            


class TableauJeu :
    ''' Cree une grille de puissance 4'''
    def __init__(self) :
        # colonnes de la grille (ou tableau) de jeu contenant 6 cases chacunes
        col0 = [[CaseJeu(3),
                 CaseJeu(4),
                 CaseJeu(5),
                 CaseJeu(5),
                 CaseJeu(4),
                 CaseJeu(3)]]
        col1 = [[CaseJeu(4),
                 CaseJeu(6),
                 CaseJeu(8),
                 CaseJeu(8),
                 CaseJeu(6),
                 CaseJeu(4)]]
        col2 = [[CaseJeu(5),
                 CaseJeu(8),
                 CaseJeu(11),
                 CaseJeu(11),
                 CaseJeu(8),
                 CaseJeu(5)]]
        col3 = [[CaseJeu(7),
                 CaseJeu(10),
                 CaseJeu(13),
                 CaseJeu(13),
                 CaseJeu(10),
                 CaseJeu(7)]]
        col4 = [[CaseJeu(5),
                 CaseJeu(8),
                 CaseJeu(11),
                 CaseJeu(11),
                 CaseJeu(8),
                 CaseJeu(5)]]
        col5 = [[CaseJeu(4),
                 CaseJeu(6),
                 CaseJeu(8),
                 CaseJeu(8),
                 CaseJeu(6),
                 CaseJeu(4)]]
        col6 = [[CaseJeu(3),
                 CaseJeu(4),
                 CaseJeu(5),
                 CaseJeu(5),
                 CaseJeu(4),
                 CaseJeu(3)]]
        # Attribut représentant la grille entière
        self.total = col0 + col1 + col2 + col3 + col4 + col5 + col6
        # Attribut donnant le niveau de la prochaine case disponible
        # dans chaque colonne
        self.niveau_colonne = [0,0,0,0,0,0,0]
        # Attribut stockant le numéro du tour
        self.compte_tours = 1
        # On créé directement les joueurs, leurs attributs seront
        # changés en fonction des décisions prises par l'utilisateur
        # dans la vue
        self.joueur_a = Joueur("Joueur 1", "A", "humain")
        self.joueur_b = Joueur("Joueur 2", "B", "humain")
        # Attributs permettant de savoir si la partie est finie
        self.partie_finie = False
        self.egalite = False
        self.gagnant = None
    def imprime_grille(self) :
        # Fonction utile uniquement si il n'y a pas d'interface graphique
        '''Imprime la grille : (0,0) sera en bas a gauche'''
        resultat = [[],[],[],[],[],[]]
        for i in range(0,6) :
            for j in range(0,7) :
                resultat[i].append(self.total[j][i])
        for k in range(len(resultat),0,-1) :
            print(resultat[k-1])
    def imprime_poids(self) :
        resultat = [[],[],[],[],[],[]]
        for i in range(0,6) :
            for j in range(0,7) :
                poids = self.total[j][i].poids
                resultat[i].append(poids)
        for k in range(len(resultat),0,-1) :
            print(resultat[k-1])
    def imprime_priorite(self) :
        resultat = [[],[],[],[],[],[]]
        for i in range(0,6) :
            for j in range(0,7) :
                resultat[i].append(self.total[j][i].priorite)
        for k in range(len(resultat),0,-1) :
            print(resultat[k-1])

    def ajout_jeton(self, num_col, joueur) :
        '''Ajout d'un jeton dans une colonne'''
        if self.niveau_colonne[num_col] > 5 :
            print("Colonne pleine")
        else:
            self.total[num_col][self.niveau_colonne[num_col]].changement_etat(joueur)
            self.niveau_colonne[num_col] += 1
            self.compte_tours += 1

    # Fonctions de vérification de l'état de la grille
    def verif_ligne(self) :
        '''Change la valeur de self.gagnant en le nom d'un des
        joueurs si celui-ci gagne grace à une ligne'''
        for i in range(0,6) : 
            for j in range (0,4) :
                cond1 = (self.total[j][i].etat == "R")
                cond2 = (self.total[j+1][i].etat == "R")
                cond3 = (self.total[j+2][i].etat == "R")
                cond4 = (self.total[j+3][i].etat== "R")
                cond5 = (self.total[j][i].etat == "J")
                cond6 = (self.total[j+1][i].etat == "J")
                cond7 = (self.total[j+2][i].etat == "J")
                cond8 = (self.total[j+3][i].etat == "J")
                joueur_a_gagne = (cond1 and cond2 and cond3 and cond4)
                joueur_b_gagne = (cond5 and cond6 and cond7 and cond8)
                if joueur_a_gagne :
                    self.partie_finie = True
                    self.gagnant = str(self.joueur_a.nom)
                elif joueur_b_gagne :
                    self.partie_finie = True
                    self.gagnant = str(self.joueur_b.nom)
                else :
                    pass
    def verif_colonne(self) :
        '''Change la valeur de self.gagnant en le nom d'un des
        joueurs si celui-ci gagne grace à une colonne'''
        for j in range(0,7) :
            for i in range(0,3) :
                cond1 = (self.total[j][i].etat == "R")
                cond2 = (self.total[j][i+1].etat == "R")
                cond3 = (self.total[j][i+2].etat == "R")
                cond4 = (self.total[j][i+3].etat == "R")
                cond5 = (self.total[j][i].etat == "J")
                cond6 = (self.total[j][i+1].etat == "J")
                cond7 = (self.total[j][i+2].etat == "J")
                cond8 = (self.total[j][i+3].etat == "J")
                joueur_a_gagne = (cond1 and cond2 and cond3 and cond4)
                joueur_b_gagne = (cond5 and cond6 and cond7 and cond8)
                if joueur_a_gagne :
                    self.partie_finie = True
                    self.gagnant = str(self.joueur_a.nom)
                elif joueur_b_gagne :
                    self.partie_finie = True
                    self.gagnant = str(self.joueur_b.nom)
                else :
                    pass

    
    def verif_diagonale_bas(self) : 
        # diagonale allant d'en haut à gauche à en bas à droite
        '''Change la valeur de self.gagnant en le nom d'un des
        joueurs si celui-ci gagne grace à une diagonalee en haut
        a gauche a en bas a droite'''
        for i in range(3,6) :
            for j in range(0,4) :
                cond1 = (self.total[j][i].etat == "R")
                cond2 = (self.total[j+1][i-1].etat == "R")
                cond3 = (self.total[j+2][i-2].etat == "R")
                cond4 = (self.total[j+3][i-3].etat == "R")
                cond5 = (self.total[j][i].etat == "J")
                cond6 = (self.total[j+1][i-1].etat == "J")
                cond7 = (self.total[j+2][i-2].etat == "J")
                cond8 = (self.total[j+3][i-3].etat == "J")
                joueur_a_gagne = (cond1 and cond2 and cond3 and cond4)
                joueur_b_gagne = (cond5 and cond6 and cond7 and cond8)
                if joueur_a_gagne :
                    self.partie_finie = True
                    self.gagnant = str(self.joueur_a.nom)
                elif joueur_b_gagne :
                    self.partie_finie = True
                    self.gagnant = str(self.joueur_b.nom)
                else :
                    pass
 
    def verif_diagonale_haut(self) :
        # diagonale allant d'en bas à gauche à en haut à droite
        '''Change la valeur de self.gagnant en le nom d'un des
        joueurs si celui-ci gagne grace à une diagonalee allant
        d'en bas à gauche à en haut à droite'''
        for i in range(3,6) :
            for j in range(3,7) :
                cond1 = (self.total[j][i].etat == "R")
                cond2 = (self.total[j-1][i-1].etat == "R")
                cond3 = (self.total[j-2][i-2].etat == "R")
                cond4 = (self.total[j-3][i-3].etat == "R")
                cond5 = (self.total[j][i].etat == "J")
                cond6 = (self.total[j-1][i-1].etat == "J")
                cond7 = (self.total[j-2][i-2].etat == "J")
                cond8 = (self.total[j-3][i-3].etat == "J")
                joueur_a_gagne = (cond1 and cond2 and cond3 and cond4)
                joueur_b_gagne = (cond5 and cond6 and cond7 and cond8)
                if joueur_a_gagne :
                    self.partie_finie = True
                    self.gagnant = str(self.joueur_a.nom)
                elif joueur_b_gagne :
                    self.partie_finie = True
                    self.gagnant = str(self.joueur_b.nom)
                else :
                    pass

    def verif_egalite(self) :
        '''Change la valeur de self.egalité si la grille
        de jeu est pleine'''
        if not self.partie_finie :
            # On utilise la variable self.compte_tours
            if self.compte_tours > 42 :
                self.egalite = True
    def determine_coup_ia(self) :
        def verif_priorite_absolue() :
            def verif_priorite_absolue_colonne(n, couleur) :
                nombre_alignements = 0
                verif1 = [-3, -2, -1]
                verif2 = [-2, -1, 1]
                verif3 = [-1, 1, 2]
                verif4 = [1, 2, 3]
                verifications = [verif1, verif2, verif3, verif4]
                for verif in verifications :
                    if verif[0] + self.niveau_colonne[n] in range(0,6) and verif[2] + self.niveau_colonne[n] in range(0,6) :
                        cond = (self.total[n][self.niveau_colonne[n]+verif[0]].etat == couleur)
                        cond = cond and (self.total[n][self.niveau_colonne[n]+verif[1]].etat == couleur)
                        cond = cond and (self.total[n][self.niveau_colonne[n]+verif[2]].etat == couleur)
                        if cond :
                            nombre_alignements += 1
                    else :
                        pass
                return nombre_alignements
            def verif_priorite_absolue_ligne(n, couleur) :
                nombre_alignements = 0
                verif1 = [-3, -2, -1]
                verif2 = [-2, -1, 1]
                verif3 = [-1, 1, 2]
                verif4 = [1, 2, 3]
                verifications = [verif1, verif2, verif3, verif4]
                for verif in verifications :
                    if verif[0] + n in range(0,7) and verif[2] + n in range(0,7) :
                        cond = (self.total[n+verif[0]][self.niveau_colonne[n]].etat == couleur)
                        cond = cond and (self.total[n+verif[1]][self.niveau_colonne[n]].etat == couleur)
                        cond = cond and (self.total[n+verif[2]][self.niveau_colonne[n]].etat == couleur)
                        if cond :
                            nombre_alignements += 1
                    else :
                        pass
                return nombre_alignements
            def verif_priorite_absolue_diagonale_haut(n, couleur) :
                nombre_alignements = 0
                verif1 = [-3, -2, -1]
                verif2 = [-2, -1, 1]
                verif3 = [-1, 1, 2]
                verif4 = [1, 2, 3]
                verifications = [verif1, verif2, verif3, verif4]
                for verif in verifications :
                    conditions_ = verif[0] + self.niveau_colonne[n] in range(0,6)
                    conditions_ = conditions_ and verif[2] + self.niveau_colonne[n] in range(0,6)
                    conditions_ = conditions_ and n+verif[0] in range(0,7)
                    conditions_ = conditions_ and n+verif[2] in range(0,7)
                    if conditions_ :
                        cond = (self.total[n+verif[0]][self.niveau_colonne[n]+verif[0]].etat == couleur)
                        cond = cond and (self.total[n+verif[1]][self.niveau_colonne[n]+verif[1]].etat == couleur)
                        cond = cond and (self.total[n+verif[2]][self.niveau_colonne[n]+verif[2]].etat == couleur)
                        if cond :
                            nombre_alignements += 1
                    else :
                        pass
                return nombre_alignements
            def verif_priorite_absolue_diagonale_bas(n, couleur) :
                nombre_alignements = 0
                verif1 = [-3, -2, -1]
                verif2 = [-2, -1, 1]
                verif3 = [-1, 1, 2]
                verif4 = [1, 2, 3]
                verifications = [verif1, verif2, verif3, verif4]
                for verif in verifications :
                    conditions_ =self.niveau_colonne[n] - verif[0] in range(0,6)
                    conditions_ = conditions_ and self.niveau_colonne[n] - verif[2] in range(0,6)
                    conditions_ = conditions_ and n+verif[0] in range(0,7)
                    conditions_ = conditions_ and n+verif[2] in range(0,7)
                    if conditions_ :
                        cond = (self.total[n+verif[0]][self.niveau_colonne[n]-verif[0]].etat == couleur)
                        cond = cond and (self.total[n+verif[1]][self.niveau_colonne[n]-verif[1]].etat == couleur)
                        cond = cond and (self.total[n+verif[2]][self.niveau_colonne[n]-verif[2]].etat == couleur)
                        if cond :
                            nombre_alignements += 1
                    else :
                        pass
                return nombre_alignements
            liste_priorites= [0,0,0,0,0,0,0]
            for couleur in ("R","J") :
                if self.joueur_a.type == "ia" and couleur == "R" :
                    ajout = 10
                elif self.joueur_b.type == "ia" and couleur == "J" :
                    ajout = 10
                else :
                    ajout = 1
                for n in range(0,7) :
                    if self.cases_disponibles[n] is not None :
                        priorite = verif_priorite_absolue_ligne(n, couleur)
                        priorite += verif_priorite_absolue_colonne(n, couleur)
                        priorite += verif_priorite_absolue_diagonale_haut(n, couleur)
                        priorite += verif_priorite_absolue_diagonale_bas(n, couleur)
                        liste_priorites[n] += priorite * ajout
                    else :
                        pass
            return liste_priorites
        def changement_des_poids() :
            def debut_alignement_ligne(n, couleur) :
                ajout_poids = 0
                verif1 = [-3, -2, -1]
                verif2 = [-2, -1, 1]
                verif3 = [-1, 1, 2]
                verif4 = [1, 2, 3]
                verifications = [verif1, verif2, verif3, verif4]
                possibilite1 = [couleur, "v", "v"]
                possibilite2 = ["v", couleur, "v"]
                possibilite3 = ["v", "v", couleur]
                une_piece = [possibilite1, possibilite2, possibilite3] 
                possibilite4 = [couleur, couleur, "v"]
                possibilite5 = [couleur, "v" ,couleur]
                possibilite6 = ["v", couleur, couleur]
                deux_pieces = [possibilite4, possibilite5, possibilite6] 
                etat_autour_case = []
                for verif in verifications :
                    if verif[0] + n in range(0,7) and verif[2] + n in range(0,7) :
                        etat_autour_case.append(self.total[n+verif[0]][self.niveau_colonne[n]].etat)
                        etat_autour_case.append(self.total[n+verif[1]][self.niveau_colonne[n]].etat)
                        etat_autour_case.append(self.total[n+verif[2]][self.niveau_colonne[n]].etat)
                        if etat_autour_case in deux_pieces :
                            ajout_poids += AJOUT_POIDS_2
                            print("ligne : + 10 ", verif)
                        elif etat_autour_case in une_piece :
                            ajout_poids += AJOUT_POIDS_1
                            print("ligne : + 2 ", verif)
                    else :
                        pass
                return ajout_poids
            def debut_alignement_colonne(n, couleur) :
                ajout_poids = 0
                verif1 = [-3, -2, -1]
                verif2 = [-2, -1, 1]
                verif3 = [-1, 1, 2]
                verif4 = [1, 2, 3]
                verifications = [verif1, verif2, verif3, verif4]
                possibilite1 = [couleur, "v", "v"]
                possibilite2 = ["v", couleur, "v"]
                possibilite3 = ["v", "v", couleur]
                une_piece = [possibilite1, possibilite2, possibilite3] 
                possibilite4 = [couleur, couleur, "v"]
                possibilite5 = [couleur, "v" ,couleur]
                possibilite6 = ["v", couleur, couleur]
                deux_pieces = [possibilite4, possibilite5, possibilite6]
                etat_autour_case = []
                for verif in verifications :
                    if verif[0] + self.niveau_colonne[n] in range(0,6) and verif[2] + self.niveau_colonne[n] in range(0,6) :
                        etat_autour_case.append(self.total[n][self.niveau_colonne[n]+verif[0]].etat)
                        etat_autour_case.append(self.total[n][self.niveau_colonne[n]+verif[1]].etat)
                        etat_autour_case.append(self.total[n][self.niveau_colonne[n]+verif[2]].etat)
                        if etat_autour_case in deux_pieces :
                            ajout_poids += AJOUT_POIDS_2
                            print("colonne : + 10 ", verif)
                        elif etat_autour_case in une_piece :
                            ajout_poids += AJOUT_POIDS_1
                            print("colonne : + 2 ", verif)
                    else :
                        pass
                return ajout_poids
            def debut_alignement_diagonale_haut(n, couleur) :
                ajout_poids = 0
                verif1 = [-3, -2, -1]
                verif2 = [-2, -1, 1]
                verif3 = [-1, 1, 2]
                verif4 = [1, 2, 3]
                verifications = [verif1, verif2, verif3, verif4]
                possibilite1 = [couleur, "v", "v"]
                possibilite2 = ["v", couleur, "v"]
                possibilite3 = ["v", "v", couleur]
                une_piece = [possibilite1, possibilite2, possibilite3] 
                possibilite4 = [couleur, couleur, "v"]
                possibilite5 = [couleur, "v" ,couleur]
                possibilite6 = ["v", couleur, couleur]
                deux_pieces = [possibilite4, possibilite5, possibilite6]
                etat_autour_case = []
                for verif in verifications :
                    conditions_ = verif[0] + self.niveau_colonne[n] in range(0,6)
                    conditions_ = conditions_ and verif[2] + self.niveau_colonne[n] in range(0,6)
                    conditions_ = conditions_ and n+verif[0] in range(0,7)
                    conditions_ = conditions_ and n+verif[2] in range(0,7)
                    if conditions_ :
                        etat_autour_case.append(self.total[n+verif[0]][self.niveau_colonne[n]+verif[0]].etat)
                        etat_autour_case.append(self.total[n+verif[1]][self.niveau_colonne[n]+verif[1]].etat)
                        etat_autour_case.append(self.total[n+verif[2]][self.niveau_colonne[n]+verif[2]].etat)
                        if etat_autour_case in deux_pieces :
                            ajout_poids += AJOUT_POIDS_2
                            print("diagonale haut : + 10 ", verif)
                        elif etat_autour_case in une_piece :
                            ajout_poids += AJOUT_POIDS_1
                            print("diagonale haut : + 2 ", verif)
                    else :
                        pass
                return ajout_poids
            def debut_alignement_diagonale_bas(n, couleur) :
                ajout_poids = 0
                verif1 = [-3, -2, -1]
                verif2 = [-2, -1, 1]
                verif3 = [-1, 1, 2]
                verif4 = [1, 2, 3]
                verifications = [verif1, verif2, verif3, verif4]
                possibilite1 = [couleur, "v", "v"]
                possibilite2 = ["v", couleur, "v"]
                possibilite3 = ["v", "v", couleur]
                une_piece = [possibilite1, possibilite2, possibilite3] 
                possibilite4 = [couleur, couleur, "v"]
                possibilite5 = [couleur, "v" ,couleur]
                possibilite6 = ["v", couleur, couleur]
                deux_pieces = [possibilite4, possibilite5, possibilite6]
                etat_autour_case = []
                for verif in verifications :
                    conditions_ =self.niveau_colonne[n] - verif[0] in range(0,6)
                    conditions_ = conditions_ and self.niveau_colonne[n] - verif[2] in range(0,6)
                    conditions_ = conditions_ and n+verif[0] in range(0,7)
                    conditions_ = conditions_ and n+verif[2] in range(0,7)
                    if conditions_ :
                        etat_autour_case.append(self.total[n+verif[0]][self.niveau_colonne[n]-verif[0]].etat)
                        etat_autour_case.append(self.total[n+verif[1]][self.niveau_colonne[n]-verif[1]].etat)
                        etat_autour_case.append(self.total[n+verif[2]][self.niveau_colonne[n]-verif[2]].etat)
                        if etat_autour_case in deux_pieces :
                            ajout_poids += AJOUT_POIDS_2
                            print("diagonale bas : + 10 ", verif)
                        elif etat_autour_case in une_piece :
                            ajout_poids += AJOUT_POIDS_1
                            print("diagonale bas : + 2 ", verif)
                    else :
                        pass
                return ajout_poids

            poids_ponctuels = [0,0,0,0,0,0,0]
            ajout_poids_total =0
            for couleur in ("R","J") :
                print("\n\n ------------------ COULEUR : ", couleur, "\n ------------------ \n")
                for n in range(0,7) :
                    print("------- COLONNE : ", n, "-------")
                    if self.niveau_colonne[n] <= 5 :
                        ajout_poids_total += debut_alignement_ligne(n, couleur)
                        ajout_poids_total += debut_alignement_colonne(n, couleur)
                        ajout_poids_total += debut_alignement_diagonale_haut(n, couleur)
                        ajout_poids_total += debut_alignement_diagonale_bas(n, couleur)
                        poids = self.total[n][self.niveau_colonne[n]].poids + ajout_poids_total
                        poids_ponctuels[n] += poids
                    else :
                        pass
            return poids_ponctuels
                    



                
        self.coup_ia = None
        self.cases_disponibles = []
        for n in range(0,7) :
            try :
                self.cases_disponibles.append(self.total[n][self.niveau_colonne[n]+1])
            except IndexError :
                self.cases_disponibles.append(None)
        if self.compte_tours == 1 :
            print("Coup IA déterminé => début de partie (colonne du milieu)")
            self.coup_ia = 3
        else :
            liste_priorites = verif_priorite_absolue()
            if max(liste_priorites) > 0 :
                self.coup_ia = liste_priorites.index(max(liste_priorites))
                print("Coup IA déterminé => alignement de 3")
            else :
                poids_cases = changement_des_poids()
                self.coup_ia = poids_cases.index(max(poids_cases))
                
                print("Coup IA déterminé => poids des cases")
                poids_depart = []
                for i in range(0,7) :
                    try :
                        poids_depart.append(self.total[i][self.niveau_colonne[i]].poids)
                    except IndexError :
                        poids_depart.append(0)
                print("Poids de depart : ", poids_depart)
                print("Poids final",poids_cases)
            if self.coup_ia is None :
                self.coup_ia = randint(0,6)
                while self.niveau_colonne[self.coup_ia] > 5 :
                    self.coup_ia = randint(0,6)
                print("Coup IA random")
            


