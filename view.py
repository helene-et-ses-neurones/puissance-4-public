from tkinter import *
from models import *
from controller import *
import threading



class ViewPreparation :
    '''La première fenetre qui apparait : on y choisit le type de partie
    (deux humains ou humain contre IA) ainsi que les noms des joueurs
    humains'''
    def __init__(self, control) :
        # Récupération de l'instance du controleur
        self.controleur = control
        # Création de la fenetre
        self.fenetre = Tk()
        self.fenetre.geometry("500x500")
        self.fenetre.title("Choix de la partie")
        # Choix humain ou IA
        self.label_general = Label(self.fenetre,
                                   text = "Contre qui jouez-vous ?")
        self.option_humain_ia = StringVar()
        self.radio_ia = Radiobutton(self.fenetre,
                               text="Je joue contre une IA",
                               value="IA",
                               variable = self.option_humain_ia)
        self.radio_humain = Radiobutton(self.fenetre,
                                   text="Nous sommes deux humains à jouer",
                                   value="H",
                                   variable = self.option_humain_ia)
        self.radio_ia.place(x=50, y=50)
        self.radio_humain.place(x=50, y=250)
        # Bouton de validation
        self.bouton_valider_humain_ia = Button(self.fenetre,
                                          text="Valider le type de partie",
                                          command = self.choix_joueurs)
        self.bouton_valider_humain_ia.place(x=225, y=450)
        self.fenetre.mainloop()

    def choix_joueurs(self) :
        '''Commande du bouton 'Valider le type de partie'. Permet
        de choisir le nom des joueurs humains et de choisir le joueur
        qui commence, y compris lorsque l'on joue contre une IA.'''
        # Fonctions de configuration des joueurs
        def nommer_joueurs_humains() :
            '''Validation du choix des joueurs humains'''
            # Modification des attributs nom des joueurs dans le modèle
            self.controleur.joueur_a.nom = self.entree_joueur_a.get()
            self.controleur.joueur_b.nom = self.entree_joueur_b.get()
            # Désactivation des entrées pour les noms des joueurs
            # et du bouton de validation
            self.entree_joueur_a.config(state= "disabled")
            self.entree_joueur_b.config(state= "disabled")
            self.bouton_valider_choix_joueurs.config(state= "disabled")
            # Ouverture de la fenetre de jeu
            self.fenetre_grille = ViewGrille(self.controleur, "humains")
        def nommer_joueur_contre_ia() :
            '''Validation du choix de l'ordre des joueurs et du
            nom du joueur humain'''
            # Si l'IA commence
            if self.option_qui_commence.get() == "IA_starts" :
                # Modification des attributs nom des joueurs dans le modèle
                self.controleur.joueur_a.nom = "L'intelligence artificielle"
                self.controleur.joueur_b.nom = self.entree_nommer_joueur_humain.get()
                # Modification de l'attribut type du joueur ia dans le modèle
                self.controleur.joueur_a.type = "ia"
                # Désactivation des widgets 
                self.radio_ia_commence.config(state= "disabled")
                self.radio_humain_commence.config(state= "disabled")
                self.bouton_valider_choix_joueurs.config(state= "disabled")
                self.entree_nommer_joueur_humain.config(state= "disabled")
                # Ouverture de la fenetre de jeu
                self.fenetre_grille = ViewGrille(self.controleur, "IA1")
            # Si l'humain commence
            elif self.option_qui_commence.get() == "H_starts" :
                # Modification des attributs nom des joueurs dans le modèle
                self.controleur.joueur_b.nom = "L'intelligence artificielle"
                self.controleur.joueur_a.nom = self.entree_nommer_joueur_humain.get()
                # Modification de l'attribut type du joueur ia dans le modèle
                self.controleur.joueur_b.type = "ia"
                # Désactivation des widgets 
                self.radio_ia_commence.config(state= "disabled")
                self.radio_humain_commence.config(state= "disabled")
                self.bouton_valider_choix_joueurs.config(state= "disabled")
                # Ouverture de la fenetre de jeu
                self.fenetre_grille = ViewGrille(self.controleur, "IA2")
            # Si aucun radiobouton n'est séléctionné
            else :
                # Fenetre de message 
                self.no_option_qui_commence = ViewProbleme("Veuillez selectionner une option")
        # Si c'est une partie contre une IA
        if self.option_humain_ia.get() == "IA" :
            # Désactivation des widgets 
            self.radio_ia.config(state= "disabled")
            self.radio_humain.config(state= "disabled")
            self.bouton_valider_humain_ia.configure(state = "disabled")
            self.label_choix_ordre_joueurs = Label(self.fenetre,
                                                   text = "Qui jouera en premier ?")
            self.label_choix_ordre_joueurs.place(x=80, y=80)
            # Choix du joueur qui commence : humain ou IA 
            self.option_qui_commence = StringVar()
            self.radio_ia_commence = Radiobutton(self.fenetre,
                                                 text="L'IA joue en premier",
                                                 value="IA_starts",
                                                 variable = self.option_qui_commence)
            self.radio_humain_commence = Radiobutton(self.fenetre,
                                                     text="Je joue en premier",
                                                     value="H_starts",
                                                     variable = self.option_qui_commence)
            self.radio_ia_commence.place(x=100, y=100)
            self.radio_humain_commence.place(x=100, y=120)
            # Entrée pour nommer le joueur humain
            self.label_nommer_joueur_humain = Label(self.fenetre,
                                                    text = "Quel est votre nom ?")
            self.label_nommer_joueur_humain.place(x=80, y=150)
            self.entree_nommer_joueur_humain = Entry(self.fenetre)
            self.entree_nommer_joueur_humain.place(x=100, y=180)
            # Bouton de validation des choix
            self.bouton_valider_choix_joueurs = Button(self.fenetre,
                                                       text="Valider les choix",
                                                       command = nommer_joueur_contre_ia)
            self.bouton_valider_choix_joueurs.place(x=200, y=210)
        # Si c'est une partie entre deux humains
        elif self.option_humain_ia.get() == "H" :
            # Désactivation des widgets
            self.radio_ia.config(state= "disabled")
            self.radio_humain.config(state= "disabled")
            self.bouton_valider_humain_ia.config(state= "disabled")
            # Entrée pour nommer les joueurs
            self.label_noms_joueurs = Label(self.fenetre,
                                            text = "Quels sont les noms des joueurs ?")
            self.label_noms_joueurs.place(x=80,y=280)
            self.label_joueur_a = Label(self.fenetre,
                                   text = "Joueur 1 : ",
                                   bg = "red")
            self.label_joueur_a.place(x=80, y=310)
            self.entree_joueur_a = Entry(self.fenetre)
            self.label_joueur_b = Label(self.fenetre,
                                   text = "Joueur 2 : ",
                                   bg = "yellow")
            self.label_joueur_b.place(x=80, y=340)
            self.entree_joueur_b = Entry(self.fenetre)
            self.entree_joueur_a.place(x=150, y=308)
            self.entree_joueur_b.place(x=150, y=338)
            # Bouton de validation des choix
            self.bouton_valider_choix_joueurs = Button(self.fenetre,
                                                  text="Valider le choix des joueurs",
                                                  command = nommer_joueurs_humains)
            self.bouton_valider_choix_joueurs.place(x=200,y=370)
        # Si aucun radiobouton n'est séléctionné    
        else :
            #Fenetre de message
            self.no_option_humain_ia_selected = ViewProbleme("Veuillez selectionner une option")



class ViewProbleme :
    '''Création d'une fenetre contenant un message en cas de
    problème'''
    def __init__(self, message) :
        # Message
        self.probleme = str(message)
        # Création de la fenetre
        self.fenetre = Tk()
        self.fenetre.title("Problème")
        self.label_message_colonne_pleine = Label(self.fenetre,
                                                 text = self.probleme)
        self.label_message_colonne_pleine.pack()
        # Bouton pour quitter la fenetre
        self.bouton_quitter_message = Button(self.fenetre,
                                            text = "OK",
                                            command = self.fenetre.destroy)
        self.bouton_quitter_message.pack()


class ViewFinPartie :
    '''Création d'une fenetre contenant un message en cas de
    problème'''
    def __init__(self, message_de_fin) : #b0, b1, b2, b3, b4, b5, b6
        # Message
        message = str(message_de_fin)
        # Création de la fenetre
        fenetre_fin = Tk()
        fenetre_fin.title("Partie terminée ! ")
        label_de_fin = Label(fenetre_fin,
                                  text = message)
        # Bouton pour quitter la fenetre
        label_de_fin.pack()
        bouton_quitter_message = Button(fenetre_fin,
                                            text = "OK",
                                            command = fenetre_fin.destroy)
        bouton_quitter_message.pack()
        # Désactivation des boutons permettant l'ajout de boutons
        #b0.config(state= "disabled")
        #b1.config(state= "disabled")
        #b2.config(state= "disabled")
        #b3.config(state= "disabled")
        #b4.config(state= "disabled")
        #b5.config(state= "disabled")
        #b6.config(state= "disabled")
        

class ViewGrille :
    '''Création de la vue de la grille'''
    def __init__(self, control, option) :
        #### EFFACER 
        print(option)
        # Récupération de l'instance du controleur
        self.controleur = control
        #
        self.colonne_pleine = [False, False, False, False, False, False, False]
        # Création de la fenetre contenant la grille
        self.fenetre = Tk()
        self.texte_tour_de_jouer_debut = "Tour #"+str(self.controleur.grille.compte_tours)+"\n C'est à "+str(self.controleur.joueur_a.nom)+" de jouer"
        self.label_tour_de_jouer = Label(self.fenetre,
                                          text = self.texte_tour_de_jouer_debut,
                                          bg = "red")
        self.label_tour_de_jouer.pack()
        self.fenetre.geometry('850x750')
        self.grille_bleue=Canvas(self.fenetre,
                                 width=720,
                                 heigh=620,
                                 bg="blue")
        self.grille_bleue.pack()
        # Création des cases dans la vue
        for ligne in range(7) :
            x_0 = 20 + ligne*100
            x_1 = 100 + ligne*100
            for colonne in range(6) :
                y_0 = 20 + colonne*100
                y_1 = 100 + colonne*100
                self.grille_bleue.create_oval(x_0,
                                              y_0,
                                              x_1,
                                              y_1,
                                              fill = "white")
        # Si deux humains jouent
        if option == "humains" :
            # Création des boutons permettant l'ajout d'un bouton
            self.bouton_ajout_0 = Button(self.fenetre,
                                        text = "AJOUTER",
                                        command = self.action_humain_view_0)
            self.bouton_ajout_0.place(x=75, y=670)
            self.bouton_ajout_1 = Button(self.fenetre,
                                        text = "AJOUTER",
                                        command = self.action_humain_view_1)
            self.bouton_ajout_1.place(x=175, y=670)
            self.bouton_ajout_2 = Button(self.fenetre,
                                        text = "AJOUTER",
                                        command = self.action_humain_view_2)
            self.bouton_ajout_2.place(x=275, y=670)
            self.bouton_ajout_3 = Button(self.fenetre,
                                        text = "AJOUTER",
                                        command = self.action_humain_view_3)
            self.bouton_ajout_3.place(x=375, y=670)
            self.bouton_ajout_4 = Button(self.fenetre,
                                         text = "AJOUTER",
                                         command = self.action_humain_view_4)
            self.bouton_ajout_4.place(x=475, y=670)
            self.bouton_ajout_5 = Button(self.fenetre,
                                        text = "AJOUTER",
                                        command = self.action_humain_view_5)
            self.bouton_ajout_5.place(x=575, y=670)
            self.bouton_ajout_6 = Button(self.fenetre,
                                        text = "AJOUTER",
                                        command = self.action_humain_view_6)
            self.bouton_ajout_6.place(x=675, y=670)
        # Si l'IA joue en premier
        elif option == "IA1" :
            # Création des boutons de jeu pour l'humain
            # les boutons sont inactifs au début
            self.bouton_ajout_0 = Button(self.fenetre,
                                        text = "AJOUTER",
                                        command = self.action_humain_contre_ia_view_0,
                                        state = "disabled")
            self.bouton_ajout_0.place(x=75, y=670)
            self.bouton_ajout_1 = Button(self.fenetre,
                                        text = "AJOUTER",
                                        command = self.action_humain_contre_ia_view_1,
                                         state = "disabled")
            self.bouton_ajout_1.place(x=175, y=670)
            self.bouton_ajout_2 = Button(self.fenetre,
                                        text = "AJOUTER",
                                        command = self.action_humain_contre_ia_view_2,
                                         state = "disabled")
            self.bouton_ajout_2.place(x=275, y=670)
            self.bouton_ajout_3 = Button(self.fenetre,
                                        text = "AJOUTER",
                                        command = self.action_humain_contre_ia_view_3,
                                         state = "disabled")
            self.bouton_ajout_3.place(x=375, y=670)
            self.bouton_ajout_4 = Button(self.fenetre,
                                         text = "AJOUTER",
                                         command = self.action_humain_contre_ia_view_4,
                                         state = "disabled")
            self.bouton_ajout_4.place(x=475, y=670)
            self.bouton_ajout_5 = Button(self.fenetre,
                                        text = "AJOUTER",
                                        command = self.action_humain_contre_ia_view_5,
                                         state = "disabled")
            self.bouton_ajout_5.place(x=575, y=670)
            self.bouton_ajout_6 = Button(self.fenetre,
                                        text = "AJOUTER",
                                        command = self.action_humain_contre_ia_view_6,
                                         state = "disabled")
            self.bouton_ajout_6.place(x=675, y=670)
            # L'IA joue
            self.action_ia_view()
            
        # Si IA joue en deuxième
        elif option == "IA2" :
            # Création des boutons de jeu pour l'humain
            self.bouton_ajout_0 = Button(self.fenetre,
                                        text = "AJOUTER",
                                        command = self.action_humain_contre_ia_view_0)
            self.bouton_ajout_0.place(x=75, y=670)
            self.bouton_ajout_1 = Button(self.fenetre,
                                        text = "AJOUTER",
                                        command = self.action_humain_contre_ia_view_1)
            self.bouton_ajout_1.place(x=175, y=670)
            self.bouton_ajout_2 = Button(self.fenetre,
                                        text = "AJOUTER",
                                        command = self.action_humain_contre_ia_view_2)
            self.bouton_ajout_2.place(x=275, y=670)
            self.bouton_ajout_3 = Button(self.fenetre,
                                        text = "AJOUTER",
                                        command = self.action_humain_contre_ia_view_3)
            self.bouton_ajout_3.place(x=375, y=670)
            self.bouton_ajout_4 = Button(self.fenetre,
                                         text = "AJOUTER",
                                         command = self.action_humain_contre_ia_view_4)
            self.bouton_ajout_4.place(x=475, y=670)
            self.bouton_ajout_5 = Button(self.fenetre,
                                        text = "AJOUTER",
                                        command = self.action_humain_contre_ia_view_5)
            self.bouton_ajout_5.place(x=575, y=670)
            self.bouton_ajout_6 = Button(self.fenetre,
                                        text = "AJOUTER",
                                        command = self.action_humain_contre_ia_view_6)
            self.bouton_ajout_6.place(x=675, y=670)
        else :
            pass
        self.fenetre.mainloop()
    def action_ia_view(self) :
        ''' Ajout d'un jeton par l'IA'''
        # Le prochain coup de l'IA est déterminé dans le modèle
        self.controleur.grille.determine_coup_ia()
        # Le jeton est ajouté
        self.ajout_jeton_view(self.controleur.grille.coup_ia)
        # Si la partie n'est pas finie, la vue est actualisée
        if not self.controleur.grille.partie_finie :
            self.actualiser_tour_de_jouer_view()
            # Les boutons de jeu du joueur humain sont réactivés
            self.bouton_ajout_0.config(state = "normal")
            self.bouton_ajout_1.config(state = "normal")
            self.bouton_ajout_2.config(state = "normal")
            self.bouton_ajout_3.config(state = "normal")
            self.bouton_ajout_4.config(state = "normal")
            self.bouton_ajout_5.config(state = "normal")
            self.bouton_ajout_6.config(state = "normal")
        # Verification : la partie est-elle finie ?
        elif self.controleur.grille.gagnant is not None :
            message_de_fin = "PARTIE TERMINEE : "+str(self.controleur.grille.gagnant)+" a gagné"
            self.fin = ViewFinPartie(message_de_fin)
            print(str(self.controleur.grille.gagnant)," a gagné")
        elif self.controleur.grille.egalite :
            message_de_fin = "PARTIE TERMINEE : Égalité"
            self.fin = ViewFinPartie(message_de_fin)
        else :
            print("else")
            
            

    def action_humain_contre_ia_view(self, num_col) :
        ''' Ajout d'un jeton par un humain lors d'une partie
        contre une IA'''
        # Le jeton est ajouté
        self.ajout_jeton_view(num_col)
        if self.colonne_pleine[num_col] == False :
            # Les boutons sont désactivés
            self.bouton_ajout_0.config(state = "disabled")
            self.bouton_ajout_1.config(state = "disabled")
            self.bouton_ajout_2.config(state = "disabled")
            self.bouton_ajout_3.config(state = "disabled")
            self.bouton_ajout_4.config(state = "disabled")
            self.bouton_ajout_5.config(state = "disabled")
            self.bouton_ajout_6.config(state = "disabled")
            # Verification : la partie est-elle finie ?
            self.verif_fin_partie_view()
            # Si la partie n'est pas finie, la vue est actualisée
            # Et l'IA joue
            if not self.controleur.grille.partie_finie :
                self.actualiser_tour_de_jouer_view()
                threading.Timer(1, self.action_ia_view).start()
    def action_humain_contre_ia_view_0(self) :
        self.action_humain_contre_ia_view(0)
    def action_humain_contre_ia_view_1(self) :
        self.action_humain_contre_ia_view(1)
    def action_humain_contre_ia_view_2(self) :
        self.action_humain_contre_ia_view(2)
    def action_humain_contre_ia_view_3(self) :
        self.action_humain_contre_ia_view(3)
    def action_humain_contre_ia_view_4(self) :
        self.action_humain_contre_ia_view(4)
    def action_humain_contre_ia_view_5(self) :
        self.action_humain_contre_ia_view(5)
    def action_humain_contre_ia_view_6(self) :
        self.action_humain_contre_ia_view(6)

    def action_humain_view(self, num_col) :
        self.ajout_jeton_view(num_col)
        self.verif_fin_partie_view()
        if not self.controleur.grille.partie_finie :
            self.actualiser_tour_de_jouer_view()
        


        
    def ajout_jeton_view(self, num_col) :
        '''Ajout visuel d'un jeton dans une colonne'''
        # Gestion d'exception : colonne pleine
        if self.controleur.grille.niveau_colonne[num_col] > 5 :
            self.fenetre_colonne_pleine = ViewProbleme("Cette colonne est pleine, \n veuillez en choisir une autre")
            self.colonne_pleine[num_col] = True
        # Si colonne non-pleine
        else :
            # Définition du joueur qui joue et de la couleur du jeton
            if self.controleur.grille.compte_tours % 2 == 1 :
                couleur = "red"
                joueur = self.controleur.joueur_a.ordre
            else :
                couleur = "yellow"
                joueur = self.controleur.joueur_b.ordre
            # Coordonnées du cercle à ajouter
            x_0 = 20 + num_col*100
            y_0 = 620 - (20 + self.controleur.grille.niveau_colonne[num_col] *100)
            x_1 = 100 + num_col*100
            y_1 = 620 - (100 + self.controleur.grille.niveau_colonne[num_col] *100)
            # Création du cercle
            self.grille_bleue.create_oval(x_0,
                                          y_0,
                                          x_1,
                                          y_1,
                                          fill = couleur)
            # Ajout d'un jeton dans le modèle grace au controleur
            self.controleur.ajout_jeton_control(num_col, joueur)
    # Fonctions d'ajout de jeton pour chaque colonne         
    def action_humain_view_0(self) :
        '''Ajout d'un jeton dans la colonne 0'''
        self.action_humain_view(0)
    def action_humain_view_1(self) :
        '''Ajout d'un jeton dans la colonne 1'''
        self.action_humain_view(1)
    def action_humain_view_2(self) :
        '''Ajout d'un jeton dans la colonne 2'''
        self.action_humain_view(2)
    def action_humain_view_3(self) :
        '''Ajout d'un jeton dans la colonne 3'''
        self.action_humain_view(3)
    def action_humain_view_4(self) :
        '''Ajout d'un jeton dans la colonne 4'''
        self.action_humain_view(4)
    def action_humain_view_5(self) :
        '''Ajout d'un jeton dans la colonne 5'''
        self.action_humain_view(5)
    def action_humain_view_6(self) :
        '''Ajout d'un jeton dans la colonne 6'''
        self.action_humain_view(6)

    def verif_fin_partie_view(self) :
        '''Affiche un message si la partie est termiée'''
        # Vérification : Est-ce que quelqu'un a gagné ?
        if self.controleur.grille.gagnant is not None :
            message_de_fin = "PARTIE TERMINEE : "+str(self.controleur.grille.gagnant)+" a gagné"
            self.fin = ViewFinPartie(message_de_fin)
            print(str(self.controleur.grille.gagnant)," a gagné")
        # Vérification : Est-ce que la grille est pleine ?
        elif self.controleur.grille.egalite :
            message_de_fin = "PARTIE TERMINEE : Égalité"
            self.fin = ViewFinPartie(message_de_fin)
        else :
            pass

    def actualiser_tour_de_jouer_view(self) :
        '''Açtualise le message affiché en haut de la
        grille : Affiche le numéro du tour et le nom du
        joueur dont c'est le tour'''
        if self.controleur.grille.partie_finie :
            pass
        elif self.controleur.grille.compte_tours%2 == 1 :
            self.texte_tour = "Tour #"+str(self.controleur.grille.compte_tours)+"\n C'est à "+str(self.controleur.joueur_a.nom)+" de jouer"
            self.label_tour_de_jouer.configure(text = self.texte_tour, bg = "red")
        else :
            self.texte_tour = "Tour #"+str(self.controleur.grille.compte_tours)+"\n C'est à "+str(self.controleur.joueur_b.nom)+" de jouer"
            self.label_tour_de_jouer.configure(text = self.texte_tour, bg = "yellow")

    
            
    

